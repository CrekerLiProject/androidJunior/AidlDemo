// ICommunicate.aidl
package com.test.aidl;

// Declare any non-default types here with import statements

interface ICommunicate {
    //客户端发送消息
    void sendInfo(String info);
    //客户端获取消息
    String getInfo();
}
