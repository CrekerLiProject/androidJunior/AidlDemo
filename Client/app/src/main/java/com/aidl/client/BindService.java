package com.aidl.client;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.test.aidl.ICommunicate;

public class BindService {

    private static final String TAG = BindService.class.getSimpleName();
    private static final String SERVICE_PACKAGE_NAME = "com.aidl.server";
    private static final String SERVICE_ACTION_NAME = "com.aidl.server.AidlService";

    private int mCounter = 0;
    private boolean mIsConnected = false;
    private ICommunicate mCommunicate;
    private Activity mActivity;

    public BindService(Activity mActivity){
        this.mActivity = mActivity;
    }

    //定义链接
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mCommunicate = ICommunicate.Stub.asInterface(service);
            mIsConnected = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mIsConnected = false;
        }
    };
    //创建绑定服务
    public void onCreate(){
        bindService();
    }
    //销毁绑定
    public void onDestroy(){
        if(mIsConnected){
            mActivity.unbindService(serviceConnection);
        }
    }
    //绑定服务
    private void bindService(){
        Intent intent = new Intent();
        //绑定Server服务
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setAction(SERVICE_ACTION_NAME);
        mActivity.bindService(intent,serviceConnection, Context.BIND_AUTO_CREATE);
    }
    //发送消息
    public void sendInfo(){
        if(mIsConnected){
            Toast.makeText(mActivity,"send " + mCounter,
                    Toast.LENGTH_SHORT).show();
            Log.d(TAG,"send " + mCounter);
            try {
                mCommunicate.sendInfo(String.valueOf(mCounter));
            } catch (RemoteException e) {
                Log.e(TAG,"send  RemoteException " + e.getMessage());
                e.printStackTrace();
            }
            mCounter++;
        }else{
            Log.d(TAG,"server disconnected ");
            Toast.makeText(mActivity,"server disconnected ",
                    Toast.LENGTH_SHORT).show();
            //实际应用中可以添加尝试重连的代码
        }
    }
    //接收消息
    public void getInfo(){
        if(mIsConnected){
            String info = "null";
            try {
                info = mCommunicate.getInfo();
            } catch (RemoteException e) {
                Log.e(TAG,"get  RemoteException  " + e.getMessage());
                e.printStackTrace();
            }

            Log.d(TAG,"get " + info);
            Toast.makeText(mActivity,"get " + info,Toast.LENGTH_SHORT).show();
        }else{
            Log.d(TAG,"server disconnected ");
            Toast.makeText(mActivity,"server disconnected ",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
