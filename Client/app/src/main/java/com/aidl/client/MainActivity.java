package com.aidl.client;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private BindService mBindService = new BindService(MainActivity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBindService.onCreate();

        Button buttonSend = findViewById(R.id.button_send);
        Button buttonGet = findViewById(R.id.button_get);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBindService.sendInfo();
            }
        });
        buttonGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBindService.getInfo();
            }
        });
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        mBindService.onDestroy();
    }



}
