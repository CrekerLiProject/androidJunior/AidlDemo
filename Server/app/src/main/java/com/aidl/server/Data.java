package com.aidl.server;

import android.util.Log;

class Data {
    private static final String TAG = Data.class.getSimpleName();
    private static Data mInstance;
    private String mInfo;

    private Data(){

    }

    public synchronized static Data getInstance(){
        if(mInstance == null){
            mInstance = new Data();
        }
        return mInstance;
    }

    String getmInfo() {
        Log.d(TAG,"get info " + mInfo);
        return mInfo;
    }

    void setmInfo(String mInfo) {
        Log.d(TAG,"set info  " + mInfo);
        this.mInfo = mInfo;
    }


}
