package com.aidl.server;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import com.test.aidl.ICommunicate;

public class AidlService extends Service {

    private Data mData = Data.getInstance();

    public AidlService() {
    }

    @Override
    public void onCreate(){
        super.onCreate();
    }

    //重写aidl中定义的两个方法
    private final ICommunicate.Stub stub = new ICommunicate.Stub() {
        @Override
        public void sendInfo(String info) throws RemoteException {
            mData.setmInfo(info);
        }

        @Override
        public String getInfo() throws RemoteException {
            return mData.getmInfo();
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return stub;
    }

}
